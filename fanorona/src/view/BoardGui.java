package view;

import algorithm.MinMax;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;
import model.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

/**
 * BoardGui is a class which build a view of application and in method main run a application.
 */
public class BoardGui extends Application {

    /**
     * Display a configure menu and get a information about game parameters from user.
     * @param logic logic of the game
     * @param controller main controller of the game
     * @return window with options in menu
     */
    static Optional<Pair<Player, Player>> playerSelectionMenu(Logic logic, FxBoardController controller) {
        Dialog<Pair<Player, Player>> dialog = new Dialog<>();
        dialog.setTitle("Game mode selection");
        dialog.setHeaderText("Select the types of players");

        ButtonType confirmButtonType = new ButtonType("Confirm", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(confirmButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 90, 10, 10));

        ChoiceBox<Player> player1Select = new ChoiceBox<>(FXCollections.observableArrayList(new Human(logic, controller, Color.WHITE), new Computer(logic, Color.WHITE)));
        ChoiceBox<Player> player2Select = new ChoiceBox<>(FXCollections.observableArrayList(new Human(logic, controller, Color.BLACK), new Computer(logic, Color.BLACK)));
        TextField selectDepthLevel_1 = new TextField();
        TextField selectDepthLevel_2 = new TextField();

        selectDepthLevel_1.setMaxWidth(60);
        selectDepthLevel_2.setMaxWidth(60);

        selectDepthLevel_1.setText(String.valueOf(MinMax.MAX_DEPTH));
        selectDepthLevel_2.setText(String.valueOf(MinMax.MAX_DEPTH));

        player1Select.setOnAction((ActionEvent event) -> {
            Object newValue = ((ChoiceBox) event.getTarget()).getValue();
            if (newValue instanceof Computer) {
                selectDepthLevel_1.setDisable(false);
            } else {
                selectDepthLevel_1.setDisable(true);
            }
        });

        player2Select.setOnAction((ActionEvent event) -> {
            Object newValue = ((ChoiceBox) event.getTarget()).getValue();
            if (newValue instanceof Computer) {
                selectDepthLevel_2.setDisable(false);
            } else {
                selectDepthLevel_2.setDisable(true);
            }
        });

        player1Select.getSelectionModel().selectFirst();
        player2Select.getSelectionModel().selectFirst();

        grid.add(new Label("White player:"), 0, 1);
        grid.add(player1Select, 1, 1);
        grid.add(new Label("Black player:"), 0, 2);
        grid.add(player2Select, 1, 2);
        grid.add(new Label("Depth Level:"), 2, 0);
        grid.add(selectDepthLevel_1, 2,1);
        grid.add(selectDepthLevel_2,2,2);

        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == confirmButtonType) {
                int deep1 = 0;
                int deep2 = 0;

                try {
                    deep1 = Integer.valueOf(selectDepthLevel_1.getText());
                } catch (NumberFormatException ignored) {
                }
                try {
                    deep2 = Integer.valueOf(selectDepthLevel_2.getText());
                } catch (NumberFormatException ignored) {
                }

                if (deep1 < 1) {
                    deep1 = MinMax.MAX_DEPTH;
                }
                if (deep2 < 1) {
                    deep2 = MinMax.MAX_DEPTH;
                }

                Player player1 = player1Select.getValue();
                Player player2 = player2Select.getValue();

                if (player1 instanceof Computer) {
                    ((Computer) player1).setMinMaxDepth(deep1);
                }
                if (player2 instanceof Computer) {
                    ((Computer) player2).setMinMaxDepth(deep2);
                }

                return new Pair<>(player1, player2);
            }
            return null;
        });

        return dialog.showAndWait();
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Start the application with parameteres from user. Show selection menu. Get the parameters for game from user.
     * Show the board with pawns. Then let's play.
     * @param primaryStage
     * @throws IOException
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        Logic logic = new Logic();

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("BoardGui.fxml"));

        Parent root = fxmlLoader.load();
        primaryStage.setScene(new Scene(root, 750, 500));
        Scene scene = primaryStage.getScene();
        scene.getStylesheets().add("resources/css/cssBoard.css");
        FxBoardController fxBoardController = fxmlLoader.getController();

        fxBoardController.setStage(primaryStage);
        fxBoardController.setLogic(logic);
        fxBoardController.updateLegalMoves(logic.legalMoves());
        fxBoardController.repaint();

        Optional<Pair<Player, Player>> selectedPlayers = playerSelectionMenu(logic, fxBoardController);

        if (!selectedPlayers.isPresent())
            return;

        Map<Color, Player> players = new LinkedHashMap<>(2);
        players.put(Color.WHITE, selectedPlayers.get().getKey());
        players.put(Color.BLACK, selectedPlayers.get().getValue());
        AtomicReference<Player> currentPlayer = new AtomicReference<>(players.get(logic.getTurn()));

        Task task = new Task<Void>() {
            @Override
            public Void call() throws InterruptedException {
                while (!logic.isEndOfGame()) {
                    currentPlayer.set(players.get(logic.getTurn()));
                    GameMove selectedMove = currentPlayer.get().selectMove();
                    logic.makeMove(selectedMove);

                    CountDownLatch block = new CountDownLatch(1);
                    Platform.runLater(() -> {
                        fxBoardController.repaint();
                        block.countDown();
                    });
                    block.await();
                }
                printPlayersStatistics(players);
                Platform.runLater(fxBoardController::endGame);
                return null;
            }
        };
        Thread gameLoop = new Thread(task);
        gameLoop.setDaemon(true);
        gameLoop.start();

        primaryStage.show();
    }

    private void printPlayersStatistics(Map<Color, Player> players) {
        for (Player player : players.values()) {
            if (player instanceof Computer) {
                double averageTime = ((Computer) player).getAverageMoveTime();
                long count = ((Computer) player).getMovesCount();

                System.out.println(player.toString() + "@" + player.getColor() + " has made " + count + " moves with average time of " + averageTime + " s");
            }
        }
    }

}
