package view;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;

/**
 *  A controller for Board view make in BoardGui.fxml
 */
public class FxBoardController {

    @FXML
    private GridPane grid = new GridPane();
    @FXML
    private Button b0 = new Button();
    @FXML
    private Button b1 = new Button();
    @FXML
    private Button b2 = new Button();
    @FXML
    private Button b3 = new Button();
    @FXML
    private Button b4 = new Button();
    @FXML
    private Button b5 = new Button();
    @FXML
    private Button b6 = new Button();
    @FXML
    private Button b7 = new Button();
    @FXML
    private Button b8 = new Button();
    @FXML
    private Button b9 = new Button();
    @FXML
    private Button b10 = new Button();
    @FXML
    private Button b11 = new Button();
    @FXML
    private Button b12 = new Button();
    @FXML
    private Button b13 = new Button();
    @FXML
    private Button b14 = new Button();
    @FXML
    private Button b15 = new Button();
    @FXML
    private Button b16 = new Button();
    @FXML
    private Button b17 = new Button();
    @FXML
    private Button b18 = new Button();
    @FXML
    private Button b19 = new Button();
    @FXML
    private Button b20 = new Button();
    @FXML
    private Button b21 = new Button();
    @FXML
    private Button b22 = new Button();
    @FXML
    private Button b23 = new Button();
    @FXML
    private Button b24 = new Button();
    @FXML
    private Button b25 = new Button();
    @FXML
    private Button b26 = new Button();
    @FXML
    private Button b27 = new Button();
    @FXML
    private Button b36 = new Button();
    @FXML
    private Button b37 = new Button();
    @FXML
    private Button b38 = new Button();
    @FXML
    private Button b39 = new Button();
    @FXML
    private Button b40 = new Button();
    @FXML
    private Button b41 = new Button();
    @FXML
    private Button b42 = new Button();
    @FXML
    private Button b43 = new Button();
    @FXML
    public Button b44 = new Button();
    @FXML
    private Label turn = new Label();
    @FXML
    private HBox hBox = new HBox();
    @FXML
    private Button changeTurn = new Button();

    private Stage primaryStage;
    private ObservableList<Node> nodeList = null;
    private ObservableList<Node> hBoxList = null;
    private Position lastClickPosition = null;
    private boolean firstClick = false;
    private boolean wasCaptured = false;
    private boolean showChangeTurn = false;
    private boolean canChangeState = false;
    private Logic logic;
    private Color lastState;

    private ArrayList<GameMove> legalMoves;
    private ArrayList<PawnMove> pawnMoves = new ArrayList<>();
    private ArrayList<PawnMove> moveAfterFirstClick = new ArrayList<>();

    private GameMove selectedMove = null;
    private CountDownLatch waiter = null;

    /**
     * Function for make a update legal moves on board.
     * @param legalMoves actual legal moves from logic
     */
    public void updateLegalMoves(ArrayList<GameMove> legalMoves) {
        pawnMoves.clear();
        for (GameMove move : legalMoves) {
            if (move instanceof PawnMove) {
                pawnMoves.add((PawnMove) move);
//                System.out.println(move);
            }
//            System.out.println();
        }

        this.legalMoves = legalMoves;
    }

    /**
     * Default constructor for FxBoardController
     */
    public FxBoardController(){}

    @FXML
    private void initialize() { }

    public void setLogic(Logic logic) {
        this.logic = logic;
    }

    /**
     * Function repaint() make a repaint of board view after player or computer move.
     */
    @FXML
    public void repaint() {
        nodeList = FXCollections.observableArrayList(grid.getChildren().sorted());
        this.sortNode(nodeList, 2, 12);
        this.sortNode(nodeList, 3, 23);
        this.sortNode(nodeList, 4, 34);
        this.sortNode(nodeList, 5, 40);
        this.sortNode(nodeList, 6, 41);
        this.sortNode(nodeList, 7, 42);
        this.sortNode(nodeList, 8, 43);
        this.sortNode(nodeList, 9, 44);

        this.clearBoard(nodeList);
        this.clearButtonStyle(nodeList);

        this.paintColor(nodeList, logic.getBlackPosition(), Color.BLACK);
        this.paintColor(nodeList, logic.getWhitePosition(), Color.WHITE);

        this.setTurnOnLabel();

        this.updateLegalMoves(logic.legalMoves());

        showPawnWithCaptured(this.pawnMoves);


        if (!logic.getTurn().equals(lastState)) {
            hBoxList = FXCollections.observableArrayList(hBox.getChildren());
            hBoxList.get(1).setStyle("-fx-background-color: whitesmoke");
        }

        showChangeTurn=false;
    }

    /**
     * Function showPawnWithCaptured draw a pawn which have a opportunity to make a capture. After first click draw
     * a pawn of opponent which we can capture.
     * @param pawnMoves list of possible moves all pawns on board
     */
    private void showPawnWithCaptured(ArrayList<PawnMove> pawnMoves) {
        for (int i = 0; i < pawnMoves.size(); i++) {
            if (pawnMoves.get(i).isCapturing()) {
                int id = pawnMoves.get(i).getInitialPosition().getX() + pawnMoves.get(i).getInitialPosition().getY() * 9;
                nodeList.get(id).setStyle("-fx-background-color: greenyellow");
            }
        }

        if (showChangeTurn && logic.getTurn().equals(lastState)) {
            hBoxList = FXCollections.observableArrayList(hBox.getChildren());
            hBoxList.get(1).setStyle("-fx-background-color: powderblue");
            System.out.println(hBoxList);
            canChangeState = true;
        }
    }

    /**
     * Function which clear all css graphic on buttons.
     * @param list a list of nodes from BoardGui.fxml
     */
    private void clearBoard(ObservableList<Node> list) {
        for (int i = 0; i < list.size(); i++) {
            Button button = (Button) list.get(i);
            button.setGraphic(null);
        }
    }

    /**
     * Function which clear all styles on buttons.
     * @param list a list of nodes from BoardGui.fxml
     */
    private void clearButtonStyle(ObservableList<Node> list) {
        for (int i = 0; i < list.size(); i++) {
            Button button = (Button) list.get(i);
            button.setStyle(null);
        }
    }

    private void setTurnOnLabel() {
        if (logic.getTurn().equals(Color.BLACK)) {
            turn.setText("Black");
        } else {
            turn.setText("White");
        }

    }

    /**
     * Function which draw a pawns on board.
     * @param list a list of nodes from BoardGui.fxml
     * @param positionColor a list with position of pawns
     * @param color color of pawns
     * @return
     */
    private ObservableList<Node> paintColor(ObservableList<Node> list, ArrayList<Position> positionColor, Color color) {

        for (int counter = 0; counter < positionColor.size(); counter++) {
            int id = positionColor.get(counter).getX() + positionColor.get(counter).getY() * 9;
            Button button = (Button) list.get(id);
            if (color.equals(Color.BLACK))
                button.setGraphic(new ImageView("resources/Images/Pionek_B.png"));
            else
                button.setGraphic(new ImageView("resources/Images/Pionek_CZ.png"));
            list.get(id).setVisible(true);
        }

        return list;
    }

    /**
     * Function to sort a nodes from BoardGui.fxml.
     * @param list  a list of nodes from BoardGui.fxml
     * @param placeTo
     * @param placeFrom
     * @return sorted list with id of nodes in good order
     */
    private ObservableList<Node> sortNode(ObservableList<Node> list, int placeTo, int placeFrom) {
        list.add(placeTo, list.get(placeFrom));
        list.remove(++placeFrom);

        return list;
    }

    @FXML
    private void changeTurnAction(ActionEvent event){

        if(canChangeState)
        {
            System.out.println("You have changed the state.");
            for (GameMove legalMove : legalMoves) {
                if (legalMove instanceof ChangeTurnMove) {
                    selectedMove = legalMove;
                    waiter.countDown();
                    break;
                }
            }
            canChangeState = false;
            showChangeTurn = false;
            hBoxList = FXCollections.observableArrayList(hBox.getChildren());
            hBoxList.get(1).setStyle("-fx-background-color: whitesmoke");
        } else{
            System.out.println("You can't have changed the state.");
        }
    }

    /**
     * Handle of button action.
     * @param event event from button
     */
    @FXML
    private void onPawnClick(ActionEvent event) {
        if (waiter == null || waiter.getCount() <= 0) return;

        Button button = (Button) event.getSource();
        lastClickPosition = id2Position(button.getId());
        System.out.println(lastClickPosition);

        if (!firstClick) {
            firstClick = firstButtonClicked();
        } else if (firstClick) {
            secondButtonClicked();
            repaint();
        }
    }

    /**
     * Handle a situation when the game was finished after the win.
     */
    public void endGame() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("End of the game");

        Optional<Color> winner = logic.getWinner();

        alert.setHeaderText(winner.get() + " won the game!");
        alert.setContentText("Do you want to play again?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            //ObservableList<Node > prba = nodeList;
            //Stage stageOld = (Stage) this.getScene();
            primaryStage.close();
            BoardGui boardGui = new BoardGui();
            primaryStage = new Stage();
            try {
                boardGui.start(primaryStage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Platform.exit();
        }
    }

    public void selectMove(CountDownLatch waiter) {
        this.waiter = waiter;
    }

    public GameMove getMove() {
        return selectedMove;
    }

    /**
     * Handle a situation after clicking the second button (pawn).
     */
    private void secondButtonClicked() {

        for (int i = 0; i < moveAfterFirstClick.size(); i++) {
            if (moveAfterFirstClick.get(i).isCapturing()) {
                wasCaptured = true;
                for (int k = 0; k < moveAfterFirstClick.get(i).getCapturedPositions().size(); k++) {
                    if (moveAfterFirstClick.get(i).getCapturedPositions().get(k).equals(lastClickPosition)) {
                        selectedMove = moveAfterFirstClick.get(i);
                        waiter.countDown();
                        wasCaptured = false;
                        firstClick = false;
                        showChangeTurn = true;
                        lastState = logic.getTurn();
                    }
                }

            }
        }

        this.clearButtonStyle(nodeList);

        if (wasCaptured) {
            System.out.println("It was beating");
            moveAfterFirstClick.clear();
            firstClick = false;
            return;
        }

        //Normalna czesc gdy nie bylo bicia
        for (int i = 0; i < moveAfterFirstClick.size(); i++) {
            if (moveAfterFirstClick.get(i).getTargetPosition().equals(lastClickPosition)) {
                selectedMove = moveAfterFirstClick.get(i);
                waiter.countDown();
                firstClick = false;
            }
        }

        firstClick = false;
        moveAfterFirstClick.clear();
    }

    /**
     * Handle a situation after clicking the second button (pawn)
     */
    private boolean firstButtonClicked() {

        showChangeTurn = false;

        for (int counter = 0; counter < this.pawnMoves.size(); counter++) {
            if (pawnMoves.get(counter).getInitialPosition().equals(lastClickPosition)) {
                moveAfterFirstClick.add(pawnMoves.get(counter));
                firstClick = true;
            }
        }

        clearButtonStyle(this.nodeList);

        for (int i = 0; i < moveAfterFirstClick.size(); i++) {
            if (moveAfterFirstClick.get(i).isCapturing()) {
                for (int k = 0; k < moveAfterFirstClick.get(i).getCapturedPositions().size(); k++) {
                    int id = moveAfterFirstClick.get(i).getCapturedPositions().get(k).getX() + moveAfterFirstClick.get(i).getCapturedPositions().get(k).getY() * 9;
                    nodeList.get(id).setStyle("-fx-background-color: cornflowerblue");
                }

            }
        }

        return this.firstClick;
    }

    /**
     * Calculate a position on board fromm button id, which was clicked.
     * @param id id of button
     * @return position on board
     */
    private Position id2Position(String id) {
        int position = Integer.valueOf(id.substring(1));
        int y = position / 9;
        int x = position - y * 9;

        return new Position(x, y);
    }

    public void setStage(Stage stage){
        this.primaryStage = stage;
    }
}
