package algorithm;

import model.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * MinMax algorithm implementation
 * This version supports alpha-beta puring, moves sorting, and state hashing optimizations
 */
public class MinMax {
    public static final int MAX_DEPTH = 7;
    private static final boolean USE_HASHING = false;

    private int maxDepth;
    private Logic game;
    private GameMove bestPawnMove;
    private Color maximizingPlayer;
    private Color minimizingPlayer;
    private HashMap<Board.BoardState, Double> maximizerStateTable;
    private HashMap<Board.BoardState, Double> minimizerStateTable;

    public MinMax(Logic game, Color player) {
        this(game, player, MAX_DEPTH);
    }

    /**
     * @param game     logic of the game
     * @param player   color of maximizing player
     * @param maxDepth maximum depth of searching tree
     */
    public MinMax(Logic game, Color player, int maxDepth) {
        this.game = game;
        maximizingPlayer = player;
        minimizingPlayer = maximizingPlayer.opposite();
        this.maxDepth = maxDepth;
        maximizerStateTable = new HashMap<>();
        minimizerStateTable = new HashMap<>();
    }

    public void setMaxDepth(int maxDepth) {
        this.maxDepth = maxDepth;
    }

    /** Choose the best move for the maximizing player */
    public GameMove chooseMove() {
        if (USE_HASHING) {
            maximizerStateTable.clear();
            minimizerStateTable.clear();
        }
        maximizer(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, maxDepth);
        return bestPawnMove;
    }

    /** Selects move that maximizes player's score */
    private double maximizer(double alpha, double beta, int depth) {
        if (depth == 0 || game.isEndOfGame()) {
            return game.computeRating(maximizingPlayer);  // computing current game score
        }

        ArrayList<GameMove> legalMoves = game.legalMoves(maximizingPlayer);
        legalMoves.sort(new MoveComparator());
        for (GameMove move : legalMoves) {  // for every possible moves for the current player
            game.makeMove(move);  // make given move

            double min;
            Board.BoardState boardState = game.getBoardState();  // get board state
            if (USE_HASHING && maximizerStateTable.containsKey(boardState)) {  // check if that state has already occurred
                min = maximizerStateTable.get(boardState);  // use values calculated before
            } else {  // otherwise go deeper and calculate value
                if (!game.legalMoves(maximizingPlayer).isEmpty()) {  // player still can make move in the turn
                    min = maximizer(alpha, beta, depth);  // make moves until the end of turn
                } else {  // let opponent do his move
                    min = minimizer(alpha, beta, depth - 1);
                }
                if (USE_HASHING)
                    maximizerStateTable.put(boardState, min);  // remember calculated value of current state
            }
            if (alpha < min) {
                alpha = min;  // select value maximizing score
                if (depth == maxDepth) {
                    bestPawnMove = move;  // select next move to perform
                }
            }
            game.undoMove(); // restore game state before last move

            if (alpha >= beta) return beta; // beta cut-off
        }

        return alpha;  // return maximum value
    }

    /** Selects move that minimizes opponent's score */
    private double minimizer(double alpha, double beta, int depth) {
        if (depth == 0 || game.isEndOfGame()) {
            return game.computeRating(maximizingPlayer);  // computing current game score
        }

        ArrayList<GameMove> legalMoves = game.legalMoves(minimizingPlayer);
        legalMoves.sort(new MoveComparator());
        for (GameMove move : legalMoves) {  // for every possible moves for the current player
            game.makeMove(move);  // make given move

            double max;
            Board.BoardState boardState = game.getBoardState();  // get board state
            if (USE_HASHING && minimizerStateTable.containsKey(boardState)) {  // check if that state has already occurred
                max = minimizerStateTable.get(boardState);  // use values calculated before
            } else {  // otherwise go deeper and calculate value
                if (!game.legalMoves(minimizingPlayer).isEmpty()) {  // player still can make move in the turn
                    max = minimizer(alpha, beta, depth);  // make moves until the end of turn
                } else {  // let opponent do his move
                    max = maximizer(alpha, beta, depth - 1);
                }
                if (USE_HASHING)
                    minimizerStateTable.put(boardState, max);  // remember calculated value of current state
            }
            beta = Math.min(beta, max);  // select value minimizing score

            game.undoMove();  // restore game state before last move

            if (alpha >= beta) return alpha;  // alpha cut-off
        }

        return beta;  // return minimum value
    }

    /**
     * Sorts moves in the following order:
     * - moves with the highest number of captured pawns,
     * - change turn move,
     * - non-capturing moves
     */
    public class MoveComparator implements Comparator<GameMove> {
        @Override
        public int compare(final GameMove lhs, GameMove rhs) {
            if (lhs instanceof PawnMove && rhs instanceof PawnMove) {
                return Integer.compare(((PawnMove) rhs).capturedCount(), ((PawnMove) lhs).capturedCount());
            } else if (lhs instanceof PawnMove && rhs instanceof ChangeTurnMove) {
                if (((PawnMove) lhs).isCapturing()) return -1;
                else return 1;
            } else if (lhs instanceof ChangeTurnMove && rhs instanceof PawnMove) {
                if (((PawnMove) rhs).isCapturing()) return 1;
                else return -1;
            }
            return 0;
        }
    }
}
