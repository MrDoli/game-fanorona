package model;

public abstract class Player {
    private final Logic logic;
    private final Color color;

    Player(Logic logic, Color color) {
        this.logic = logic;
        this.color = color;
    }

    abstract public GameMove selectMove();

    public Color getColor() {
        return color;
    }
}
