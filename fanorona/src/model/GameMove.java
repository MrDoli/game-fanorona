package model;

abstract public class GameMove {
    final Color player;

    GameMove(Color player) {
        this.player = player;
    }
}
