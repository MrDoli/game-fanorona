package model;


import model.Node.Direction;

import java.util.ArrayList;
import java.util.Objects;

/**
 *  Represents move with its initial and target positions and captured Pawns
 */
public class PawnMove extends GameMove {
    private Position initialPosition;
    private Position targetPosition;
    private Pawn pawn;
    private ArrayList<Pawn> captured;
    private ArrayList<Position> capturedPositions;

    public PawnMove(Position initialPosition, Position targetPosition, Pawn pawn) {
        super(pawn.getColor());

        this.initialPosition = initialPosition;
        this.targetPosition = targetPosition;
        this.pawn = pawn;
        captured = new ArrayList<>();
        capturedPositions = new ArrayList<>();
    }

    public Pawn getPawn() {
        return pawn;
    }

    public Position getInitialPosition() {
        return initialPosition;
    }

    public Position getTargetPosition() {
        return targetPosition;
    }

    public void addCaptured(Pawn pawn, Position position) {
        captured.add(pawn);
        capturedPositions.add(position);
    }

    public void addCaptured(ArrayList<Pawn> pawns, ArrayList<Position> positions) {
        assert pawns.size() == positions.size();
        captured.addAll(pawns);
        capturedPositions.addAll(positions);
    }

    public ArrayList<Pawn> getCaptured() {
        return captured;
    }

    public ArrayList<Position> getCapturedPositions() {
        return capturedPositions;
    }

    public Direction direction() {
        return Direction.direction(initialPosition, targetPosition);
    }

    public Boolean isCapturing() {
        return !captured.isEmpty();
    }

    public int capturedCount() {
        return captured.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PawnMove pawnMove = (PawnMove) o;
        return initialPosition.equals(pawnMove.initialPosition) &&
                targetPosition.equals(pawnMove.targetPosition) &&
                pawn.equals(pawnMove.pawn) &&
                Objects.equals(captured, pawnMove.captured) &&
                Objects.equals(capturedPositions, pawnMove.capturedPositions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(initialPosition, targetPosition, pawn, captured, capturedPositions);
    }

    @Override
    public String toString() {
        return "PawnMove{" +
                "initialPosition=" + initialPosition +
                ", targetPosition=" + targetPosition +
                ", capturedPositions=" + capturedPositions +
                '}';
    }
}
