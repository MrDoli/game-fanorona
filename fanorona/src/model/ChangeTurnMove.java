package model;

/**
 *  Used to change turn when a capture is still possible in a turn
 */
public class ChangeTurnMove extends GameMove {
    ChangeTurnMove(Color player) {
        super(player);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        return o != null && getClass() == o.getClass();
    }

    @Override
    public String toString() {
        return "ChangeTurnMove{}";
    }
}
